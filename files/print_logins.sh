#!/bin/bash

old_num=0

rm www/login.unl.edu/data/*

while [ 1 ]; do
    new_num=`ls www/login.unl.edu/data | wc -l`
    if [ "x$new_num" != "x$old_num" ]; then
	filename=`ls www/login.unl.edu/data | head -n $new_num | tail -n 1`
	cat www/login.unl.edu/data/$filename
    fi
    old_num=$new_num
    sleep 1
done
