#!/bin/bash

num=`echo $#`

if [ "x$num" == "x1" ]; then
    password=$1
else
    echo "usage: ./generate_psk.sh PASSWORD"
    exit -1
fi

wpa_passphrase "bells of ireland" "$password"

