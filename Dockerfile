#FROM kalilinux/kali-linux-docker
FROM ubuntu:xenial
# Upgrade
#RUN apt-get dist-upgrade -y

# Update apt
RUN apt-get update --fix-missing && apt-get install -y \
    apt-utils \
    iputils-ping sudo net-tools htop \
    ssh openssh-server \
    git gitg \
    emacs gedit \
    udev \
    bash-completion usbutils \
    python-apt python-pip \
    x11-apps

#RUN apt-get update --fix-missing && apt-get dist-upgrade -y
RUN apt-get install -y apt-transport-https net-tools \
    	    	       x11-apps \
		       emacs \
		       ssh openssh-server libssl-dev libssl1.0.2\
		       git \
		       make gcc g++ \
		       libnl-genl-3-dev \
		       psmisc \
		       wpasupplicant ca-cacert libdns162 libdns-export162
#metasploit-framework asleap beef-xss firefox-esr
RUN export DEBIAN_FRONTEND=noninteractive && apt-get install -y macchanger
RUN apt-get install -y libapache2-mod-dnssd libapache2-mod-php7.0 \
		       apache2 dsniff dnsmasq python-dnspython python-pcapy python-scapy sslsplit stunnel4 tinyproxy procps iptables scapy \
		       libnl-genl-3-200 libnl-genl-3-dev \
		       pkg-config \
		       rfkill wireless-tools 

RUN apt-get install -y ruby ruby-dev
		       

# Create a new user named kali and set passwords
RUN useradd -ms /bin/bash kali
RUN echo 'kali:kali' | chpasswd 
RUN echo 'root:kali' | chpasswd
RUN echo "kali ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/kali
RUN usermod -aG sudo kali

USER kali
WORKDIR /home/kali
RUN git clone https://github.com/beefproject/beef
WORKDIR /home/kali/beef
RUN sed -i 's/^ *read .*/REPLY="y"/' install
RUN sed -i 's/apt-get install/apt-get install -y/' install
RUN sed -i 's/\( *\)\(gem.*install bundler\)/\1sudo \2/' install
RUN sudo ls

RUN export TERM=xterm && ./install

# Install mana
USER kali
WORKDIR /home/kali
RUN git clone --depth 1 https://github.com/sensepost/mana.git
WORKDIR /home/kali/mana
RUN git submodule init; git submodule update; cd hostapd-mana; git checkout hostapd-2.6; cd ..; make;
USER root
RUN make install





RUN mkdir demo

WORKDIR /home/kali/demo
RUN git clone https://code.vt.edu/cnap_cpss/manawww.git
RUN git clone https://code.vt.edu/cnap_cpss/apache2.git manaapache2

USER root
RUN ln -s /etc/apache2
RUN ln -s /etc/mana-toolkit
RUN ln -s /usr/share/mana-toolkit/run-mana
RUN ln -s /usr/share/mana-toolkit/www



# Copy in some custom stuff
USER root
RUN mv /usr/share/mana-toolkit/www /usr/share/mana-toolkit/wwwold
RUN cp -r manawww /usr/share/mana-toolkit/www

RUN cp manaapache2/sites-available/xkcd.conf /etc/apache2/sites-available/
RUN cp manaapache2/sites-available/eshop.conf /etc/apache2/sites-available/
RUN cp manaapache2/sites-available/login.unl.edu.conf /etc/apache2/sites-available/
WORKDIR /etc/apache2/sites-enabled
RUN ln -s /etc/apache2/sites-available/xkcd.conf
RUN ln -s /etc/apache2/sites-available/eshop.conf
RUN ln -s /etc/apache2/sites-available/login.unl.edu.conf

# Copy custom mana script into the image
WORKDIR /home/kali/demo
USER kali
COPY files/* /home/kali/

USER root
COPY sysfiles/dnsmasq-dhcpd.conf /home/kali/demo/mana-toolkit/dnsmasq-dhcpd.conf
RUN chown kali:kali -R /home/kali/*
RUN chmod 777 /home/kali/demo/www/login.unl.edu/data

USER root
RUN echo "X11UseLocalHost yes" >> /etc/ssh/sshd_config
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
RUN echo "Port 2022" >> /etc/ssh/sshd_config
RUN echo "AddressFamily any" >> /etc/ssh/sshd_config
RUN echo "ListenAddress 0.0.0.0" >> /etc/ssh/sshd_config
RUN echo "ListenAddress ::" >> /etc/ssh/sshd_config

# Create symbolic links for convenience
WORKDIR /home/kali
USER kali
RUN echo "export PATH=$PATH:/sbin" >> .bashrc
RUN sed -i 's/\( *passwd:\).*/\1 "feeb"/' /home/kali/beef/config.yaml 
USER root
# Run some commands at the beginning
ENTRYPOINT service ssh restart && \
	   service dbus start && \
	   service avahi-daemon start && \
	   service apache2 restart && \
	   echo "nameserver 8.8.8.8" >> /etc/resolv.conf && \
	   service networking restart  && \
	   sudo -H -u kali bash -c 'cd /home/kali/beef && sudo ./beef'

#	   cd /home/kali && ./start-nat-custom.sh && \



