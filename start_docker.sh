echo "Make sure that you have killed network-manager and used a connection script before you continue.  Would you like to continue? [y/N]"

read yesno

if [ "x$yesno" != "xy" ]; then
    exit
fi


id=`sudo docker container ls -a | grep mitm | sed 's/ .*//'`

if [ "x$id" != "x" ]; then
    echo -n "Docker container mitm already exists, should I kill the old container [y/N]: "
    read ret
    if [ "x$ret" == "xy" ] || [ "x$ret" == "xY" ]; then
	echo "removing container"
	sudo docker rm -f $id
    elif [ "x$ret" == "xn" ] || [ "x$ret" == "xN" ]; then
	echo -n ""
    elif [ "x$ret" == "x" ]; then
	 exit
    else
	echo "answer not recognized ... exiting"
    fi
fi

sudo docker run -d --name=mitm --privileged -v $PWD/working:/home/kali/working --net="host" -i -t mitm 

num=`cat ~/.ssh/config | grep mitm | wc -l`

if [ "x$num" == "x0" ]; then
    echo "Adding ssh config for kali"
    echo "Host mitm" >> ~/.ssh/config
    echo "     HostName 127.0.0.1" >> ~/.ssh/config
    echo "     User kali" >> ~/.ssh/config
    echo "     Port 2022" >> ~/.ssh/config
fi


echo "Container created"
echo "To log in to the container run 'ssh -X mitm'"




