#!/bin/bash



wlan0=`ifconfig | grep HWaddr | grep wlp | sed 's/ .*//'`

if [ "x$wlan0" == "x" ]; then
    wlan0=`ifconfig | grep HWaddr | grep wlo | sed 's/ .*//'`
fi

if [ "x$wlan0" == "x" ]; then
    wlan0=`ifconfig | grep HWaddr | grep wls | sed 's/ .*//'`
fi



wlan1=`ifconfig | grep HWaddr | grep wlx | sed 's/ .*//'`


if [ "x$wlan0" == "x" ]; then
    echo "ERROR: could not find internal wireless"
    return
fi

if [ "x$wlan1" == "x" ]; then
    echo "ERROR: could not find external wireless"
    return
fi
echo $wlan0
echo $wlan1

export wlan0=$wlan0
export wlan1=$wlan1
